-- phpMyAdmin SQL Dump
-- version 4.6.6deb5ubuntu0.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Jan 20, 2022 at 03:57 PM
-- Server version: 5.7.36-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `discussion110621`
--

-- --------------------------------------------------------

--
-- Table structure for table `faculty`
--

CREATE TABLE `faculty` (
  `id` int(11) NOT NULL,
  `user_name` varchar(200) NOT NULL,
  `user_email` varchar(200) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(100) DEFAULT NULL,
  `joining_date` varchar(100) NOT NULL,
  `login_date` date DEFAULT NULL,
  `logout_date` date DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1 = logged in',
  `eventname` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `faculty`
--

INSERT INTO `faculty` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`) VALUES
(1, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021/06/08 17:05:00', '2021-06-08', '2021-06-08', 1, 'Abbott'),
(2, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021/06/08 18:16:44', '2021-06-08', '2021-06-08', 1, 'Abbott'),
(3, 'Arunkumar Ranganathan', 'arunkumar.r@abbott.com', 'Chennai', 'Abbott Vascular', NULL, NULL, '2021/06/11 17:50:15', '2021-06-11', '2021-06-11', 1, 'Abbott'),
(4, 'Arunkumar Ranganathan', 'arunkumar.r@abbott.com', 'Chennai', 'Abbott Vascular', NULL, NULL, '2021/06/11 18:30:35', '2021-06-11', '2021-06-11', 1, 'Abbott'),
(5, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021/06/11 18:35:54', '2021-06-11', '2021-06-11', 1, 'Abbott'),
(6, 'Aashish Chopra', 'chopraaashish@hotmail.com', 'Chennai', 'MMM', NULL, NULL, '2021/06/11 18:43:07', '2021-06-11', '2021-06-11', 1, 'Abbott'),
(7, 'V.NANDHAKUMAR', 'nandhacard2013@gmail.com', 'Chennai', 'MMM', NULL, NULL, '2021/06/11 18:43:32', '2021-06-11', '2021-06-11', 1, 'Abbott'),
(8, 'Srinivasan N ', 'cardiodoc2006@gmail.com', 'Chennai', 'MMM', NULL, NULL, '2021/06/11 18:47:03', '2021-06-11', '2021-06-11', 1, 'Abbott'),
(9, 'V.NANDHAKUMAR', 'nandhacard2013@gmail.com', 'Chennai', 'MMM', NULL, NULL, '2021/06/11 18:51:36', '2021-06-11', '2021-06-11', 1, 'Abbott'),
(10, 'Aashish Chopra', 'chopraaashish@hotmail.com', 'Chennai', 'MMM', NULL, NULL, '2021/06/11 18:51:57', '2021-06-11', '2021-06-11', 1, 'Abbott'),
(11, 'kalaichelvan uthayakuaran', 'ukalaichelvan77@yahoo.com', 'chennai', 'MMM', NULL, NULL, '2021/06/11 18:54:53', '2021-06-11', '2021-06-11', 1, 'Abbott'),
(12, 'Nandhakumar', 'nandhapaed1982@gmail.com', 'Chennai', 'MMM', NULL, NULL, '2021/06/11 19:00:24', '2021-06-11', '2021-06-11', 1, 'Abbott'),
(13, 'DR P BALAJI', 'drbalajip@gmail.com', 'Chennai', 'MADRAS MEDICAL MISSION', NULL, NULL, '2021/06/11 19:01:03', '2021-06-11', '2021-06-11', 1, 'Abbott'),
(14, 'Aashish Chopra ', 'chopraaashish@hotmail.com', 'Chennai', 'MMm', NULL, NULL, '2021/06/11 19:11:24', '2021-06-11', '2021-06-11', 1, 'Abbott'),
(15, 'Aashish Chopra', 'chopraaashish@hotmail.com', 'Chennai', 'MMM', NULL, NULL, '2021/06/11 19:48:10', '2021-06-11', '2021-06-11', 1, 'Abbott'),
(16, 'Aashish Chopra', 'chopraaashish@hotmail.com', 'Chennai', 'MMM', NULL, NULL, '2021/06/11 19:48:11', '2021-06-11', '2021-06-11', 1, 'Abbott'),
(17, 'Aashish Chopra', 'chopraaashish@hotmail.com', 'Chennai', 'MMM', NULL, NULL, '2021/06/11 19:59:24', '2021-06-11', '2021-06-11', 1, 'Abbott'),
(18, 'Aashish Chopra', 'chopraaashish@hotmail.com', 'Chennai', 'MMM', NULL, NULL, '2021/06/11 20:28:00', '2021-06-11', '2021-06-11', 1, 'Abbott'),
(19, 'Aashish', 'chopraaashish@hotmail.com', 'Chennai', 'MMM', NULL, NULL, '2021/06/11 20:31:01', '2021-06-11', '2021-06-11', 1, 'Abbott');

-- --------------------------------------------------------

--
-- Table structure for table `faculty_new`
--

CREATE TABLE `faculty_new` (
  `id` int(50) NOT NULL,
  `Name` varchar(200) NOT NULL,
  `EmailID` varchar(200) NOT NULL,
  `City` varchar(200) NOT NULL,
  `Hospital` varchar(200) NOT NULL,
  `Last Login On` varchar(200) NOT NULL,
  `Logout Times` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_questions`
--

CREATE TABLE `tbl_questions` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(255) NOT NULL,
  `user_question` varchar(500) NOT NULL,
  `asked_at` datetime NOT NULL,
  `eventname` varchar(255) NOT NULL,
  `speaker` int(11) NOT NULL DEFAULT '0',
  `answered` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_users`
--

CREATE TABLE `tbl_users` (
  `id` int(11) NOT NULL,
  `user_name` varchar(255) NOT NULL,
  `user_email` varchar(500) NOT NULL,
  `city` varchar(100) NOT NULL,
  `hospital` varchar(100) NOT NULL,
  `speciality` varchar(100) DEFAULT NULL,
  `reg_no` varchar(50) DEFAULT NULL,
  `joining_date` datetime NOT NULL,
  `login_date` datetime DEFAULT NULL,
  `logout_date` datetime DEFAULT NULL,
  `logout_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=loggedin',
  `eventname` varchar(255) NOT NULL,
  `token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_users`
--

INSERT INTO `tbl_users` (`id`, `user_name`, `user_email`, `city`, `hospital`, `speciality`, `reg_no`, `joining_date`, `login_date`, `logout_date`, `logout_status`, `eventname`, `token`) VALUES
(1, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-06-08 17:04:32', '2021-06-08 17:04:32', '2021-06-08 17:04:40', 0, 'Abbott', 'fb0ab84450cefd2c960ff6a4c70445c7'),
(2, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-06-08 18:13:25', '2021-06-08 18:13:25', '2021-06-08 18:15:35', 0, 'Abbott', '96a594705c44d07faf817cd33980fd08'),
(3, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-06-11 17:58:20', '2021-06-11 17:58:20', '2021-06-11 17:58:56', 0, 'Abbott', '62ad5d84d5964f66caabf890790f7bda'),
(4, 'Suresh kumar', 'ksureshdr@gmail.com', 'Vellore', 'Apollo Kh ', NULL, NULL, '2021-06-11 18:42:44', '2021-06-11 18:42:44', '2021-06-11 20:12:44', 0, 'Abbott', 'fb428a98142e4ef3634edbe8710b51ce'),
(5, 'Arunkumar R', 'arunkumar.r@abbott.com', 'Chennai', 'Abbott vascular', NULL, NULL, '2021-06-11 18:50:39', '2021-06-11 18:50:39', '2021-06-11 20:20:39', 0, 'Abbott', 'eda2a0caa4f0ca9caf08272af5195714'),
(6, 'Gowtham.R', 'rajigowthaman@gmail.com', 'Chennai', 'Abbott', NULL, NULL, '2021-06-11 18:50:50', '2021-06-11 18:50:50', '2021-06-11 20:20:50', 0, 'Abbott', 'f3fb6f8fb5332831ceaf3e2a12794041'),
(7, 'Udit Mathur', 'udit_m.in@jtbap.com', 'Test', 'Test', NULL, NULL, '2021-06-11 18:59:07', '2021-06-11 18:59:07', '2021-06-11 20:45:17', 0, 'Abbott', '53465b6058a9aeb6da517d70dfb418b1'),
(8, 'Shiyam ', 'shiyam.sundar@abbott.com', 'Chennai ', 'AV', NULL, NULL, '2021-06-11 19:03:14', '2021-06-11 19:03:14', '2021-06-11 20:33:14', 0, 'Abbott', 'a4168325833eeb4d1938c7a37c6c2736'),
(9, 'Nishanth.S', 'nishanth@coact.co.in', 'bangalore', 'bangl', NULL, NULL, '2021-06-11 19:05:01', '2021-06-11 19:05:01', '2021-06-11 19:05:07', 0, 'Abbott', '74b672038bc69121b5a0b4f446e9fc8c'),
(10, 'Adharsh Narain ', 'adharshnarain05@gmail.com', 'Dindigul', 'VM', NULL, NULL, '2021-06-11 19:06:07', '2021-06-11 19:06:07', '2021-06-11 20:36:07', 0, 'Abbott', '13e797c12f97979f144a4da2003ead04'),
(11, 'Dr Dinesh ', 'dineshmbbs2005@yahoo.co.in', 'Chennai', 'MMM', NULL, NULL, '2021-06-11 19:07:08', '2021-06-11 19:07:08', '2021-06-11 20:37:08', 0, 'Abbott', '7d03be4734fbc935f9e889c17c388fd3'),
(12, 'Vignesh', 'vigneshmdcard@yahoo.com', 'Chennai', 'Mmm', NULL, NULL, '2021-06-11 19:07:45', '2021-06-11 19:07:45', '2021-06-11 20:37:45', 0, 'Abbott', '82995a644e5a77d7feb268d97d2e66e4'),
(13, 'Dr Sathish ', 'drsathishkumar2001@yahoo.com', 'Chennai', 'Frontier lifeline', NULL, NULL, '2021-06-11 19:08:45', '2021-06-11 19:08:45', '2021-06-11 20:38:45', 0, 'Abbott', 'e5a6c54eed3056b030ec1d4b2ec8ff5e'),
(14, 'Giridharan ', 'Girigirias@gmail.com', 'Pondicherry ', 'MGM', NULL, NULL, '2021-06-11 19:08:57', '2021-06-11 19:08:57', '2021-06-11 20:38:57', 0, 'Abbott', 'f88050b7d8b64efafeb7e7ccb05e2249'),
(15, 'Dr Krishnamoorthy ', 'drkrishnamoorthycard@yahoo.co.in', 'Chennai', 'Frontier lifeline', NULL, NULL, '2021-06-11 19:09:26', '2021-06-11 19:09:26', '2021-06-11 20:39:26', 0, 'Abbott', 'd4d13420b44667549705d91e623b51ec'),
(16, 'Radha', 'Radhapri@gmail.com', 'Chennai', 'Apollo Hospital ', NULL, NULL, '2021-06-11 19:09:39', '2021-06-11 19:09:39', '2021-06-11 20:39:39', 0, 'Abbott', '6e6a71cbe62d15cfb25b54f28aafa53a'),
(17, 'Ashok', 'gashokmkumar@gmail.com', 'Chennai', 'Chettinad Hospital ', NULL, NULL, '2021-06-11 19:10:20', '2021-06-11 19:10:20', '2021-06-11 20:40:20', 0, 'Abbott', '8bf1d7855beb36d706752042ec6217b4'),
(18, 'D Anandkumar', 'ganandkumar14@gmail.cm', 'Chennai', 'Mmm', NULL, NULL, '2021-06-11 19:10:39', '2021-06-11 19:10:39', '2021-06-11 20:40:39', 0, 'Abbott', '33162e7e5e6a55b8ca95c18ec4056a09'),
(19, 'Lavanya', 'lavi4321@gmail.com', 'Chennai ', 'Apollo', NULL, NULL, '2021-06-11 19:10:54', '2021-06-11 19:10:54', '2021-06-11 20:40:54', 0, 'Abbott', '80305a3645daae66094444649ee41137'),
(20, 'Karthikeyan ', 'Karthikeyandm@gmail.com', 'Chennai', 'Prashanth Hospital ', NULL, NULL, '2021-06-11 19:11:30', '2021-06-11 19:11:30', '2021-06-11 20:41:30', 0, 'Abbott', '9f5d74dee58210f5176df61c53c2f722'),
(21, 'Harish', 'harish2356@gmail.com', 'Chennai', 'Chennai', NULL, NULL, '2021-06-11 19:11:58', '2021-06-11 19:11:58', '2021-06-11 20:41:58', 0, 'Abbott', '8fc678655f295af0deea3273c85a8853'),
(22, 'Avinash', 'Avinbox@yahoo.co.in', 'Pondicherry ', 'JIPMER', NULL, NULL, '2021-06-11 19:12:33', '2021-06-11 19:12:33', '2021-06-11 20:42:33', 0, 'Abbott', '188ef70aea783b19e05f560d62ce0ef8'),
(23, 'Dr Balavignesh', 'drbalavigneshmd@gmail.com', 'Vellore', 'Naruvi', NULL, NULL, '2021-06-11 19:13:01', '2021-06-11 19:13:01', '2021-06-11 20:43:01', 0, 'Abbott', '9c3c3c4c355a2089278cb28721b57cc9'),
(24, 'Rajesh', 'Rajegmi6232@gmail.com', 'Cjennai', 'Chennai', NULL, NULL, '2021-06-11 19:13:29', '2021-06-11 19:13:29', '2021-06-11 20:43:29', 0, 'Abbott', '4348405b8366900e3f7a49c3e2fb24aa'),
(25, 'Dr Arvind Yuvaraj', 'draravingyuvaraj@yahoo.com', 'Vellore', 'Naruvi', NULL, NULL, '2021-06-11 19:13:45', '2021-06-11 19:13:45', '2021-06-11 20:43:45', 0, 'Abbott', '55a59ee3d1d8fd2b1adab48d3277c988'),
(26, 'Sriram', 'Srivats.vee@gmail.com', 'Chennai', 'SRM', NULL, NULL, '2021-06-11 19:14:03', '2021-06-11 19:14:03', '2021-06-11 20:44:03', 0, 'Abbott', '2bb8f5b3fc39c80a28fc3005ebcf1b07'),
(27, 'Dr Ilyas ', 'drilyas_cardio@yahoo.co.in', 'Chennai', 'RGGH', NULL, NULL, '2021-06-11 19:14:49', '2021-06-11 19:14:49', '2021-06-11 20:44:49', 0, 'Abbott', 'ca72dda1c7902c2779863de7af089ea6'),
(28, 'Premnath', 'premnathvinayagm@gmail.com', 'Pondy', 'MGM', NULL, NULL, '2021-06-11 19:15:41', '2021-06-11 19:15:41', '2021-06-11 20:45:41', 0, 'Abbott', 'fb34402c3aa3ed85f06863f924f7160a'),
(29, 'Daniel K', 'danielchristoper1998@gmail.com', 'Chennai', 'Abbott', NULL, NULL, '2021-06-11 19:18:10', '2021-06-11 19:18:10', '2021-06-11 20:48:10', 0, 'Abbott', '2df62cb18c0a3df9f801edea6e3e983f'),
(30, 'Pradeep kumar', 'pradeeprajnala@gmail.com', 'Vellore', 'CMC ', NULL, NULL, '2021-06-11 19:19:18', '2021-06-11 19:19:18', '2021-06-11 20:49:18', 0, 'Abbott', '13b5d69c0b12026a07ffe121be574e0d'),
(31, 'Dr Srinivasan', 'srinisadguru1978@gmail.com', 'Thanjavur', 'Meenakshi Hospital ', NULL, NULL, '2021-06-11 19:19:33', '2021-06-11 19:19:33', '2021-06-11 20:49:33', 0, 'Abbott', 'a4034d5547ba65374865a977698d90d2'),
(32, 'Dr Sujit ', 'drsujitmddm@gmail.com', 'Vellore', 'CMC ', NULL, NULL, '2021-06-11 19:20:01', '2021-06-11 19:20:01', '2021-06-11 20:50:01', 0, 'Abbott', '871d178b6c6168de26a0d8436e194fcd'),
(33, 'T Sadiyan', 'sadacathlab@gmail.com', 'Thanjavur', 'Meenakshi Hospital ', NULL, NULL, '2021-06-11 19:20:28', '2021-06-11 19:20:28', '2021-06-11 20:50:28', 0, 'Abbott', 'a21ebad9ebe1aff70fab29b0ee011c19'),
(34, 'Dr Ajeet', 'drajeetarul@yahoo.co.in', 'Chennai', 'Miot', NULL, NULL, '2021-06-11 19:20:46', '2021-06-11 19:20:46', '2021-06-11 20:50:46', 0, 'Abbott', 'c9bc8ffca1ffd0566b880f8911028bfd'),
(35, 'Dr Ramachandran', 'ramachandrandevara76@yahoo.co.in', 'Kumbakonam', 'Anbu hospital ', NULL, NULL, '2021-06-11 19:21:22', '2021-06-11 19:21:22', '2021-06-11 20:51:22', 0, 'Abbott', 'fe292218cc79b7022b159a8816f5396b'),
(36, 'Antony Wilson', 'antony.wilson87@gmail.com', 'Chennai ', 'SRMC', NULL, NULL, '2021-06-11 19:21:40', '2021-06-11 19:21:40', '2021-06-11 20:51:40', 0, 'Abbott', '052a1aa873b7191027629728b4580b52'),
(37, 'Dr S Aravind ', 'draravindsampath@yahoo.co.in', 'Chennai', 'Apollo ayanambakkam', NULL, NULL, '2021-06-11 19:21:41', '2021-06-11 19:21:41', '2021-06-11 20:51:41', 0, 'Abbott', 'b0e62dbd9f63119ea750d16516783ab0'),
(38, 'Kanna K', 'kannan_kumaresan1984@yahoo.co.in', 'Pudukkottai', 'Muthu Meenakshi Hospital ', NULL, NULL, '2021-06-11 19:22:21', '2021-06-11 19:22:21', '2021-06-11 20:52:21', 0, 'Abbott', '6f3abce0d28f1a761ad5e6a79652a78d'),
(39, 'Vimala kannan', 'vimalarajeshnkanna76@refiffmail.com', 'Karur', 'KNR Hospital ', NULL, NULL, '2021-06-11 19:23:04', '2021-06-11 19:23:04', '2021-06-11 20:53:04', 0, 'Abbott', '378aa916b4e589ef0fd5803f1bd04b36'),
(40, 'Dr Ashok victor', 'drashokcardio@yahoo.co.in', 'Chennai', 'KMC', NULL, NULL, '2021-06-11 19:23:28', '2021-06-11 19:23:28', '2021-06-11 20:53:28', 0, 'Abbott', '0797df1d74e1a2b3c4f781142f97270f'),
(41, 'Vijay Balaji', 'drvijaybala@yahoo.com', 'Chennai ', 'MIOT', NULL, NULL, '2021-06-11 19:23:50', '2021-06-11 19:23:50', '2021-06-11 20:53:50', 0, 'Abbott', '0d9137d9a09de2c0b01f97aec9be2dbd'),
(42, 'Aditya', 'draditya.aruvia@gmail.com', 'Chennai ', 'SRMC', NULL, NULL, '2021-06-11 19:25:08', '2021-06-11 19:25:08', '2021-06-11 20:55:08', 0, 'Abbott', 'ecdacdf60370eec92f9703eb211fc27b'),
(43, 'Deepesh v ', 'dr_vdeepesh@yahoo.co.in', 'Chennai', 'Apollo speciality', NULL, NULL, '2021-06-11 19:25:08', '2021-06-11 19:25:08', '2021-06-11 20:55:08', 0, 'Abbott', '0f1a6c88e8bfac2cf0fe6a951a62f8a5'),
(44, 'Ashok', 'drashokkumar@gmail.com', 'Chennai ', 'Kamakshi Memorial', NULL, NULL, '2021-06-11 19:27:01', '2021-06-11 19:27:01', '2021-06-11 20:57:01', 0, 'Abbott', '71ec3625ca47a33792c16869e306584d'),
(45, 'Sanjay', 'drsanjay.norian@gmail.com', 'Chennai ', 'SIMS', NULL, NULL, '2021-06-11 19:28:52', '2021-06-11 19:28:52', '2021-06-11 20:58:52', 0, 'Abbott', 'd4a990f56ff51d91ef0d96bfc37796a6'),
(46, 'Rufus ', 'rufusdemel85@gmail.com', 'Hyderabad ', 'Apollo', NULL, NULL, '2021-06-11 19:47:00', '2021-06-11 19:47:00', '2021-06-11 21:17:00', 0, 'Abbott', '271859818165a8fb4fb3a606dd077f89'),
(47, 'Gowtham', 'rajigowthaman@gmail.com', 'Chennai', 'Abbott', NULL, NULL, '2021-06-11 19:48:37', '2021-06-11 19:48:37', '2021-06-11 21:18:37', 0, 'Abbott', '1c11a1c63b7bd7f6c51e7d45b1812583'),
(48, 'Manoj', 'dr.manojpmc@gmail.com', 'Pudukkotai ', 'MMH', NULL, NULL, '2021-06-11 20:02:40', '2021-06-11 20:02:40', '2021-06-11 21:32:40', 0, 'Abbott', '2408b22ff57d2fd4454526ad4249db6b'),
(49, 'Daniel K', 'danielchristoper1998@gmail.com', 'Chennai', 'Abbott', NULL, NULL, '2021-06-11 20:17:15', '2021-06-11 20:17:15', '2021-06-11 21:47:15', 0, 'Abbott', '4ce455b8c3e8c0b2725fcec7d8b665d4'),
(50, 'Adharsh Narain', 'adharshnarain5@gmail.com', 'Dindigul', 'Vadamalayan', NULL, NULL, '2021-06-11 20:25:54', '2021-06-11 20:25:54', '2021-06-11 21:55:54', 0, 'Abbott', 'a21a62039498a7c457dc828f190bc566'),
(51, 'Pratik Sane', 'pratiksane89@gmail.com', 'Thane', 'Nh', NULL, NULL, '2021-06-11 20:46:10', '2021-06-11 20:46:10', '2021-06-11 20:46:32', 0, 'Abbott', 'cfc99965b6a5d61d8b31862ed31a1efe');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `faculty`
--
ALTER TABLE `faculty`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faculty_new`
--
ALTER TABLE `faculty_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_users`
--
ALTER TABLE `tbl_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `faculty`
--
ALTER TABLE `faculty`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `faculty_new`
--
ALTER TABLE `faculty_new`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_questions`
--
ALTER TABLE `tbl_questions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_users`
--
ALTER TABLE `tbl_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
